# About

I ported some styles from other parts to be used in `texstudio.ini`. Rigth now, the installation is manual by copying the code into the `.ini` file.

- **Zenburn**: I'm using the colors directly from a [zenburn port for emacs](https://github.com/bbatsov/zenburn-emacs/blob/master/zenburn-theme.el)
- **Solarized**: I'm using the style from a [tex.stackexchange answer](http://tex.stackexchange.com/a/108599/7561), which uses the [Solarized color palette](http://ethanschoonover.com/solarized).

# Install

You can use the different styles by changing the `texstudio.ini` information by replacing from `[formats]` onward.

Note that you need to close and reopen the application to make the new format take effect.

## In linux

`texstudio.ini` is under `.config/texstudio`
